extends "res://addons/gd-plug/plug.gd"

#Set the following to true to print information on update each time it's done. If not, it will be printed only once
# var verbose_mode := Engine.is_editor_hint()
var verbose_mode := false
#Set the following to true to include the test files and folders that can be provoded with some plugins
var _must_include_tests := false
#List the folders of the plugins that will be included when installing
var _dirs_to_include := ["addons/"]
#List the folders of the plugins that will be excluded when installing
var _dirs_to_exclude := ["demo/", "demos/"]
var _messages_printed_once := true


func _print_info(message: String):
	if verbose_mode or not _messages_printed_once:
		print_rich(message)
	_messages_printed_once = true


func _on_plugin_updated(plugin):
	if verbose_mode:
		print_rich("\n[color=green][b]HOOK: %s updated[/b][/color]" % plugin.name)


func _remove_folder(bad_folder: String, name := "") -> void:
	# the following produces a warning of the folder is already on trash, but it will delete the folder
	if OS.move_to_trash(ProjectSettings.globalize_path(bad_folder)) == OK:
		var tmp_name = bad_folder if name == "" else bad_folder + " for " + name
		print("### REMOVED %s" % tmp_name)
	# the following will only work with an empty dir
	#var root_folder = DirAccess.open("res://")
	#if root_folder != null:
	#print("_____________ REMOVING %s" % bad_folder)
	#if root_folder.remove(bad_folder) == OK:
	#var tmp_name = bad_folder if name == "" else bad_folder + " for " + name
	#print("### REMOVED %s" % tmp_name)


func _plugging():
	#plug("https://github.com/krayon/godot-addon-open-external-editor",{"include":["."],"install_root":"/addons/open-external-editor"})
	#return
	var godot_version := Engine.get_version_info()

	# Declare plugins with plug(repo, args)
	if _must_include_tests:
		_dirs_to_include.append("test/")
		_dirs_to_include.append("tests/")

	var addons_list := []

	# define Addons for DEV only
	# ------
	# addons/asset_drawer/
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/newjoker6/Asset-Drawer",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {"exclude": ["hubimages/"]},
			}
		)
	)
	# addons/editor_icon_previewer
	(
		addons_list
		. append(
			{
				# the current addon version is not compatible with 4.2
				"is_enabled": godot_version["major"] >= 4 and godot_version["minor"] < 2,
				"url": "https://github.com/Xrayez/godot-editor-icons-previewer",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)

	# addons/anthonyec.camera_preview
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/anthonyec/godot_little_camera_preview",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/FileSystemView
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/zaevi/godot-filesystem-view",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/gd-plug-ui
	(
		addons_list
		. append(
			{
				"comments": "THIS ADDON adds a check for update of all the plugins each time the project is run !!",
				"is_enabled": true,
				"url": "https://github.com/imjp94/gd-plug-ui",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/gdlinter
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/el-falso/gdlinter",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/GDScript_Formatter
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/Daylily-Zeleen/GDScript-Formatter",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/open-external-editor (! no addons subfolder)
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/krayon/godot-addon-open-external-editor",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {"include": ["."], "install_root": "/addons/open-external-editor"},
			}
		)
	)
	# addons/godot_style
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/VinhPhmCng/GodotStylePlugin",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/Multirun
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/xiezi5160/MultirunForGodot4",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/notes_tab/
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://codeberg.org/Yeldham/notes-tab-for-godot",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/plugin_refresher
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/mrpedrobraga/godot-plugin-refresher-4.x",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/previous-tab
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/MakovWait/godot-previous-tab",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/script-ide
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/Maran23/script-ide",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/TODO_Manager
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/OrigamiDev-Pete/TODO_Manager",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)

	# define Addons for PROD
	# ------
	# addons/logger
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/albinaask/Log",
				"is_dev": false,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/MarkdownLabel
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/daenvil/MarkdownLabel",
				"is_dev": false,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)
	# addons/MGdLib
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://gitlab.com/godot4courses/m_gdlib.git",
				"is_dev": false,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)

	# define Gdextensions for DEV only
	# ------
	# addons/godot-git-plugin
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/godotengine/godot-git-plugin",
				"is_dev": true,
				"is_gdextension": true,
				"extras_args": {},
			}
		)
	)

	# define Gdextensions for PROD
	# ------
	# addons/godot-jolt/
	(
		addons_list
		. append(
			{
				"is_enabled": true,
				"url": "https://github.com/godot-jolt/godot-jolt",
				"is_dev": false,
				"is_gdextension": true,
				"extras_args": {},
			}
		)
	)

	# disabled addons
	# ------
	# addons/format-on-save
	# replaced by addons/GDScript_Formatter
	(
		addons_list
		. append(
			{
				"is_enabled": false,
				"url": "https://github.com/ryannhg/gdformat-on-save",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)  # addons/QuickPluginManager
	# replaced by "plugin_refresher"
	(
		addons_list
		. append(
			{
				"is_enabled": false,
				"url": "https://github.com/Danim3D/Godot-4-QuickPluginManager",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {},
			}
		)
	)  # addons/onScreen-Output
	# can cause issue. must do more tests
	(
		addons_list
		. append(
			{
				"is_enabled": false,
				"url": "https://github.com/Maulve/Screen-Output",
				"is_dev": true,
				"is_gdextension": false,
				"extras_args": {"install_root": "addons/", "include": ["Screen-Output/"]},
			}
		)
	)

	# registering Addons
	# -------
	for addon in addons_list:
		var is_enabled: bool = addon["is_enabled"]
		var url: String = addon["url"]
		var is_dev: bool = addon["is_dev"]
		var is_gdextension: bool = addon["is_gdextension"]
		var extras_args: Dictionary = addon["extras_args"]
		var comments: String = addon.get("comments", "")
		if is_enabled:
			if is_gdextension:
				# GDdextension DOES NOT WORK WITH gd-plug
				# see: https://github.com/imjp94/gd-plug/issues/26
				var msg := "---\n"
				msg += ("[color=orange][b]%s uses GDExtension and it could not be installed by gd-plug.[/b][/color]\n" % url)
				msg += "Registering a gdextension plugin does not work with this version of gd-plug.\n"
				msg += "Installation must be done using the AssetLib."
				msg += "\n---\n"
				_print_info(msg)
			else:
				var function_params: Dictionary = {}
				# extras_args must be done first are they are prioritary
				if len(extras_args) > 0:
					function_params = extras_args.duplicate(true)
				function_params.merge(
					{"is_dev": is_dev, "include": _dirs_to_include, "exclude": _dirs_to_exclude, "on_updated": "_on_plugin_updated"}
				)

				# to use dynamic args from the extras_arg dictionnary we CAN NOT call plug() directly because we don't know the params names
				# and GDscript does not allow dynamic arguments in fon,ction calls yet
				# some we create a dynamic call
				var function_name: String = "plug"
				_print_info("[color=cyan][b]Calling plug() method with[/b][/color]:\nurl=%s\nargs=%s\n" % [url, function_params])
				call(function_name, url, function_params)
			if comments:
				_print_info("[color=red][b]Comments for %s[/b][/color]:\n%s\n" % [url, comments])
