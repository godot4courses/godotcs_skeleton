# Godot_Skeleton

## Description

Framework pour Godot Engine 4 (et suivants), regroupant les fonctionnalités utiles à un jeu vidéo.

## Objectifs

TODO

## Mouvements et actions du joueur

TODO

## Interactions

TODO

## Copyrights

Codé en C# en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) Laurent Ongaro / GameaMea Studio (https://www.gameamea.com)

=================================================

## Description

Base Framework for Godot Engine 3 (and more), gathering the useful features for a video game.

## Goals

TODO

## Movements and actions of the player

TODO

## Interactions

TODO

## Copyrights

Coded in C# using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) Laurent Ongaro / GameaMea Studio (https://www.gameamea.com)
