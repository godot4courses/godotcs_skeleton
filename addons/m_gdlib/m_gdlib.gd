@tool
class_name MGdLib
extends Node

## Class that handles aditionnal features and classes
## Methods can either be accessed through the "MGdLib" singleton, or you can instance this class yourself(no need to add it to the tree)

enum LogLevel {
	DEFAULT,
	DEBUG,
	INFO,
	WARN,
	ERROR,
	FATAL,
}

enum ErrorCode { NOT_INITIALIZED, NO_CONTAINER }

var is_initialized := false
var dialog_box: MGDLIB_DialogBox


func _init() -> void:
	print_rich("[color=yellow]An MGdLib instance has been created[/color]")


func init_dialog_box(container: Node):
	if not is_instance_valid(dialog_box) or not dialog_box.is_initialized:
		dialog_box = MGDLIB_DialogBox.new(container)
	is_initialized = true
